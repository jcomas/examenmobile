package com.churomobile.examenmobile.common;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public abstract class Constants {

    public static final String PAYMNET_METHOD = "payment_method";
    public static final String PAYMNET_METHOD_ID = "payment_method_id";
    public static final String BANK = "bank";
    public static final String BANK_ID = "bank_id";
    public static final String AMOUNT = "amount";
    public static final String PAYER_COST = "payer_cost";

    public static final String MP_PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";

}
