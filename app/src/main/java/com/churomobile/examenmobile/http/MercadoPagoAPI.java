package com.churomobile.examenmobile.http;

import com.churomobile.examenmobile.http.entity.Banks;
import com.churomobile.examenmobile.http.entity.Installments;
import com.churomobile.examenmobile.http.entity.PayerCost;
import com.churomobile.examenmobile.http.entity.PaymentMethods;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jecom_000 on 30/11/2018....
 */
public interface MercadoPagoAPI {

    @GET("payment_methods")
    Call<PaymentMethods> getPaymentMethods(@Query("public_key") String publicKey);

    @GET("payment_methods/card_issuers")
    Call<Banks> getBanks(@Query("public_key") String publicKey, @Query("payment_method_id") String paymentMethodId);

    @GET("payment_methods/installments")
    Call<Installments> getInstallments(@Query("public_key") String publicKey,
                                       @Query("amount") String amout,
                                       @Query("payment_method_id") String paymentMethodId,
                                       @Query("issuer.id") String issuer);
}
