package com.churomobile.examenmobile;

import com.churomobile.examenmobile.ui.amount.AmountActivity;
import com.churomobile.examenmobile.ui.bank.BankActivity;
import com.churomobile.examenmobile.dagger.AmountModule;
import com.churomobile.examenmobile.dagger.BankModule;
import com.churomobile.examenmobile.dagger.InstallmentModule;
import com.churomobile.examenmobile.dagger.MercadoPagoModule;
import com.churomobile.examenmobile.dagger.PaymentMethodModule;
import com.churomobile.examenmobile.ui.installment.InstallmentActivity;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, AmountModule.class, PaymentMethodModule.class, MercadoPagoModule.class, BankModule.class, InstallmentModule.class})
public interface ApplicationComponent {

    void inject(AmountActivity target);

    void inject(PaymentMethodActivity target);

    void inject(BankActivity target);

    void inject(InstallmentActivity target);

}
