package com.churomobile.examenmobile.dagger;

import com.churomobile.examenmobile.http.MercadoPagoAPI;
import com.churomobile.examenmobile.ui.installment.InstallmentMVP;
import com.churomobile.examenmobile.ui.installment.InstallmentModel;
import com.churomobile.examenmobile.ui.installment.InstallmentPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jecom_000 on 2/12/2018....
 */

@Module
public class InstallmentModule {

    @Provides
    public InstallmentMVP.Presenter provideInstallmentPresenter(InstallmentMVP.Model model) {
        return new InstallmentPresenter(model);
    }


    @Provides
    public InstallmentMVP.Model provideInstallmentModel(MercadoPagoAPI service) {
        return new InstallmentModel(service);
    }
}
