package com.churomobile.examenmobile.dagger;

import com.churomobile.examenmobile.http.MercadoPagoAPI;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jecom_000 on 30/11/2018....
 */

@Module
public class MercadoPagoModule {

    public final String BASE_URL = "https://api.mercadopago.com/v1/";

    @Provides
    public OkHttpClient provideHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Provides
    public Retrofit provideRetrofit(String baseUrl, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    public MercadoPagoAPI provideMercadoPagoService() {
        return provideRetrofit(BASE_URL, provideHttpClient()).create(MercadoPagoAPI.class);
    }

}
