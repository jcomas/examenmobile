package com.churomobile.examenmobile.dagger;

import com.churomobile.examenmobile.ui.amount.AmountMVP;
import com.churomobile.examenmobile.ui.amount.AmountPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AmountModule {

    @Provides
    public AmountMVP.Presenter provideAmountPresenter() {
        return new AmountPresenter();
    }

}
