package com.churomobile.examenmobile.dagger;

import com.churomobile.examenmobile.ui.bank.BankMVP;
import com.churomobile.examenmobile.ui.bank.BankModel;
import com.churomobile.examenmobile.ui.bank.BankPresenter;
import com.churomobile.examenmobile.http.MercadoPagoAPI;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jecom_000 on 2/12/2018....
 */

@Module
public class BankModule {

    @Provides
    public BankMVP.Presenter providePaymentMethodPresenter(BankMVP.Model model) {
        return new BankPresenter(model);
    }


    @Provides
    public BankMVP.Model providePaymentMethodModel(MercadoPagoAPI service) {
        return new BankModel(service);
    }

}
