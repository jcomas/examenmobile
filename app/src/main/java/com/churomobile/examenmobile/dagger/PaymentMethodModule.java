package com.churomobile.examenmobile.dagger;

import com.churomobile.examenmobile.http.MercadoPagoAPI;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodMVP;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodModel;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jecom_000 on 30/11/2018....
 */

@Module
public class PaymentMethodModule {

    @Provides
    public PaymentMethodMVP.Presenter providePaymentMethodPresenter(PaymentMethodMVP.Model model) {
        return new PaymentMethodPresenter(model);
    }


    @Provides
    public PaymentMethodMVP.Model providePaymentMethodModel(MercadoPagoAPI service) {
        return new PaymentMethodModel(service);
    }
}
