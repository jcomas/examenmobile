package com.churomobile.examenmobile.ui.installment;

import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.MercadoPagoAPI;
import com.churomobile.examenmobile.http.entity.Installments;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class InstallmentModel implements InstallmentMVP.Model {

    private final MercadoPagoAPI service;

    public InstallmentModel(MercadoPagoAPI service) {
        this.service = service;
    }

    @Override
    public void getPayerCosts(String amount, String paymentMethod, String bankIssuer, InstallmentMVP.Model.OnFinishedListener listener) {

        Call<Installments> call = this.service.getInstallments(Constants.MP_PUBLIC_KEY, amount, paymentMethod, bankIssuer);
        call.enqueue(new Callback<Installments>() {
            @Override
            public void onResponse(Call<Installments> call, Response<Installments> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<Installments> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }

}
