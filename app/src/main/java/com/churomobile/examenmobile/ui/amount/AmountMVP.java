package com.churomobile.examenmobile.ui.amount;

/**
 * Created by jecom_000 on 30/11/2018....
 */
public interface AmountMVP {

    interface View {

        String getAmount();
        void showAmountError();
        void goPaymentMethodsActivity(String amount);

        void clearData();
    }

    interface Presenter {
        void setView(AmountMVP.View view);
        String getAmount();
        void nextButtonClick();

        void clearData();
    }

    interface Model {
    }
}
