package com.churomobile.examenmobile.ui.installment;

import android.support.annotation.Nullable;

import com.churomobile.examenmobile.http.entity.Installment;
import com.churomobile.examenmobile.http.entity.PayerCost;

import java.util.ArrayList;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class InstallmentPresenter implements InstallmentMVP.Presenter, InstallmentMVP.Model.OnFinishedListener {


    @Nullable
    private InstallmentMVP.View view;
    private InstallmentMVP.Model model;

    public InstallmentPresenter(InstallmentMVP.Model model) {//dagger va a inyectar el model
        this.model = model;
    }

    @Override
    public void setView(InstallmentMVP.View view) {
        this.view = view;
    }

    @Override
    public void getPayerCosts() {
        view.showProgress();
        String paymentMethod = view.getPaymentMethod();
        String issuerBank = view.getIssuerBank();
        String amount = view.getAmount();
        if (paymentMethod == null) {
            view.showInvalidPaymentMethodError();
        } else if (issuerBank == null) {
            view.showInvalidBankError();
        } else if (amount == null) {
            view.showInvalidAmount();
        } else {
            model.getPayerCosts(amount, paymentMethod, issuerBank, this);
        }
    }

    @Override
    public void finishButtonClick() {
        if (validInstallment()) {
            view.showSuccessDialog();
        } else {
            view.showInvalidInstallmentError();
        }
    }

    private boolean validInstallment() {
        String installment = view.getInstallment();
        if (installment == null || installment.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void onFinished(ArrayList<Installment> installmentsList) {
        if (installmentsList == null || installmentsList.isEmpty()) {
            view.showNoResultError();
        } else if (installmentsList.get(0).getPayerCosts().isEmpty()) {
            view.showNoResultError();
        } else {
            ArrayList<PayerCost> list = (ArrayList<PayerCost>) installmentsList.get(0).getPayerCosts();
            view.updatePayerCosts(list);
        }
        view.hideProgress();

    }

    @Override
    public void onFailure(Throwable t) {
        view.showErrorDialog(t);
        view.hideProgress();
    }
}
