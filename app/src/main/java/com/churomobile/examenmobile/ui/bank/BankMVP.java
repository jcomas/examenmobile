package com.churomobile.examenmobile.ui.bank;

import com.churomobile.examenmobile.http.entity.Bank;

import java.util.ArrayList;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public interface BankMVP {

    interface View {

        void showProgress();
        void hideProgress();

        void updateBanks(ArrayList<Bank> banksList);

        void showErrorDialog(Throwable t);
        void showInvalidPaymentMethodError();
        void showNoResultError();

        void goInstallmentsActivity();

        String getPaymentMethod();

        String getSelectedBank();

        void showNoSelectedBankError();
    }

    interface Presenter {
        void setView(BankMVP.View view);

        void getBanks();

        void nextButtonClick();
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished(ArrayList<Bank> banksList);

            void onFailure(Throwable t);
        }

        void getBanks(String paymentMethod, OnFinishedListener listener);
    }

}
