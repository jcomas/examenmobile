package com.churomobile.examenmobile.ui.installment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.http.entity.PayerCost;

import java.util.List;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class InstallmentAdapter extends RecyclerView.Adapter<InstallmentAdapter.InstallmentHolder> {

    Context context;
    List<PayerCost> payerCostList;
    InstallmentAdapter.OnItemClickListener listener;

    public InstallmentAdapter(Context context, List<PayerCost> payerCostList, InstallmentAdapter.OnItemClickListener listener) {
        this.context = context;
        this.payerCostList = payerCostList;
        this.listener = listener;
    }

    public void setPayerCostList(List<PayerCost> payerCostList) {
        this.payerCostList = payerCostList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InstallmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.installment_adapter, parent, false);
        return new InstallmentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InstallmentHolder holder, int position) {
        holder.payerCost.setText(payerCostList.get(position).getRecommendedMessage());
        holder.bind(payerCostList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return payerCostList.size();
    }

    public class InstallmentHolder extends RecyclerView.ViewHolder {

        TextView payerCost;

        public InstallmentHolder(View itemView) {
            super(itemView);
            payerCost = itemView.findViewById(R.id.textViewPayerCost);
        }

        public void bind(final PayerCost item, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PayerCost payerCost);
    }
}
