package com.churomobile.examenmobile.ui.amount;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.churomobile.examenmobile.MyApplication;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodActivity;

import javax.inject.Inject;

/**
 * Created by jecom_000 on 30/11/2018....
 */
public class AmountActivity extends AppCompatActivity implements AmountMVP.View {

    @Inject
    AmountMVP.Presenter presenter;

    private EditText editAmount;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.amount_main);

        ((MyApplication) getApplication()).getComponent().inject(this);

        editAmount = findViewById(R.id.editTextAmount);
        nextButton = findViewById(R.id.buttonNext);
        nextButton.setOnClickListener(view -> presenter.nextButtonClick());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String amount = bundle.getString(Constants.AMOUNT);
            if (amount != null && !amount.isEmpty()) {

                String method = bundle.getString(Constants.PAYMNET_METHOD);
                String bank = bundle.getString(Constants.BANK);
                String cost = bundle.getString(Constants.PAYER_COST);

                String message = getString(R.string.final_message, amount, method, bank, cost);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(message);
                builder.setTitle(R.string.summary);
                builder.setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                    presenter.clearData();
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    public String getAmount() {
        return editAmount.getText().toString();
    }

    @Override
    public void showAmountError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.invalid_amount);
        builder.setTitle(R.string.error);
        builder.setPositiveButton(R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void goPaymentMethodsActivity(String amount) {
        Intent intent = new Intent(this, PaymentMethodActivity.class);
        intent.putExtra(Constants.AMOUNT, amount);
        startActivity(intent);
    }

    @Override
    public void clearData() {
        getIntent().putExtras(new Bundle());
        editAmount.setText("");
    }
}
