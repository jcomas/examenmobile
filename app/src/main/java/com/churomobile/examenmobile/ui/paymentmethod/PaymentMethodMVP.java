package com.churomobile.examenmobile.ui.paymentmethod;

import com.churomobile.examenmobile.http.entity.PaymentMethod;

import java.util.ArrayList;

/**
 * Created by jc on 30/11/2018....
 */
public interface PaymentMethodMVP {

    interface View {
        void showProgress();
        void hideProgress();

        void updatePaymentMethods(ArrayList<PaymentMethod> paymentMethodsArrayList);

        void showErrorDialog(Throwable t);
        void showInvalidMethodDialog();

        void goBanksActivity();

        String getSelectedPaymentMethod();


    }

    interface Presenter {
        void setView(PaymentMethodMVP.View view);
        void getPaymentMethods();

        void nextButtonClick();
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished(ArrayList<PaymentMethod> paymentMethodsArrayList);
            void onFailure(Throwable t);
        }
        void getPaymentMethods(OnFinishedListener listener);
    }
}
