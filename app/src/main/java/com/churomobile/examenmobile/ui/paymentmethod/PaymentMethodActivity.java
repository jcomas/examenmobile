package com.churomobile.examenmobile.ui.paymentmethod;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.churomobile.examenmobile.MyApplication;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.entity.PaymentMethod;
import com.churomobile.examenmobile.ui.bank.BankActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by javier comas on 30/11/2018....
 */
public class PaymentMethodActivity extends AppCompatActivity implements PaymentMethodMVP.View {

    @Inject
    PaymentMethodMVP.Presenter presenter;

    private List<PaymentMethod> pMethodList;
    private RecyclerView recyclerView;
    private PaymentMethodAdapter recyclerAdapter;
    private Button nextButton;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        ((MyApplication) getApplication()).getComponent().inject(this);

        nextButton = findViewById(R.id.buttonNext);
        nextButton.setOnClickListener(view -> presenter.nextButtonClick());
        nextButton.setVisibility(View.GONE);

        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.retrieving_payment_method));

        pMethodList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerViewPaymentMethods);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new PaymentMethodAdapter(this, pMethodList, paymentMethod -> {

            Bundle bundle = getIntent().getExtras();
            bundle.putString(Constants.PAYMNET_METHOD_ID, paymentMethod.getId());
            bundle.putString(Constants.PAYMNET_METHOD, paymentMethod.getName());
            getIntent().putExtras(bundle);

            presenter.nextButtonClick();

//            showDialog("", paymentMethod.getName());

        });
        recyclerView.setAdapter(recyclerAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.getPaymentMethods();
    }

    @Override
    public void updatePaymentMethods(ArrayList<PaymentMethod> paymentMethodsArrayList) {
        recyclerAdapter.setPaymentMethodList(paymentMethodsArrayList);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showErrorDialog(Throwable t) {
        t.printStackTrace();
        showDialog(getString(R.string.error), getString(R.string.retrieving_error));
    }

    @Override
    public void showInvalidMethodDialog() {
        showDialog(getString(R.string.error), getString(R.string.invalid_payment_method));
    }

    @Override
    public void goBanksActivity() {
        Intent intent = new Intent(this, BankActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    @Override
    public String getSelectedPaymentMethod() {
        String pm = getIntent().getStringExtra(Constants.PAYMNET_METHOD_ID);
        return pm;
    }

    @Override
    public void showProgress() {
        progress.show();
    }

    @Override
    public void hideProgress() {
        progress.dismiss();
    }

}
