package com.churomobile.examenmobile.ui.paymentmethod;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.http.entity.PaymentMethod;

import java.util.List;

/**
 * Created by jecom_000 on 1/12/2018....
 */
public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodHolder> {

    Context context;
    List<PaymentMethod> pMethodList;
    OnItemClickListener listener;

    public PaymentMethodAdapter(Context context, List<PaymentMethod> pMethodList, OnItemClickListener listener) {
        this.context = context;
        this.pMethodList = pMethodList;
        this.listener = listener;
    }

    public void setPaymentMethodList(List<PaymentMethod> pMethodList) {
        this.pMethodList = pMethodList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PaymentMethodAdapter.PaymentMethodHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.payment_method_adapter, parent, false);
        return new PaymentMethodHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodAdapter.PaymentMethodHolder viewHolder, int position) {
        viewHolder.tvPaymentMethod.setText(pMethodList.get(position).getName());
        Glide.with(context).load(pMethodList.get(position).getThumbnail()).into(viewHolder.image);
        viewHolder.bind(pMethodList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return pMethodList.size();
    }

    public class PaymentMethodHolder extends RecyclerView.ViewHolder {
        TextView tvPaymentMethod;
        ImageView image;

        public PaymentMethodHolder(View itemView) {
            super(itemView);
            tvPaymentMethod = itemView.findViewById(R.id.textViewPaymentMethod);
            image = itemView.findViewById(R.id.imageViewPaymentMethod);
        }

        public void bind(final PaymentMethod item, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PaymentMethod paymentMethod);
    }
}
