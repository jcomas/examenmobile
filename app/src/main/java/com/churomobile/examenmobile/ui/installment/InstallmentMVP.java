package com.churomobile.examenmobile.ui.installment;

import com.churomobile.examenmobile.http.entity.Installment;
import com.churomobile.examenmobile.http.entity.PayerCost;

import java.util.ArrayList;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public interface InstallmentMVP {

    interface View {

        void showProgress();
        void hideProgress();

        void updatePayerCosts(ArrayList<PayerCost> payerCostList);

        void showErrorDialog(Throwable t);
        void showInvalidPaymentMethodError();
        void showInvalidBankError();
        void showNoResultError();
        void showInvalidAmount();

        String getPaymentMethod();
        String getIssuerBank();
        String getAmount();

        void showSuccessDialog();

        String getInstallment();

        void showInvalidInstallmentError();
    }


    interface Presenter {
        void setView(InstallmentMVP.View view);
        void getPayerCosts();
        void finishButtonClick();
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished(ArrayList<Installment> installmentList);
            void onFailure(Throwable t);
        }

        void getPayerCosts(String amout, String paymentMethod, String bankIssuer, OnFinishedListener listener);
    }

}
