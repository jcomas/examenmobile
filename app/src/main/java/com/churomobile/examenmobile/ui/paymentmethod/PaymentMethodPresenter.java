package com.churomobile.examenmobile.ui.paymentmethod;

import android.support.annotation.Nullable;

import com.churomobile.examenmobile.http.entity.PaymentMethod;

import java.util.ArrayList;

/**
 * Created by jc on 30/11/2018....
 */
public class PaymentMethodPresenter implements PaymentMethodMVP.Presenter, PaymentMethodMVP.Model.OnFinishedListener {

    @Nullable
    private PaymentMethodMVP.View view;
    private PaymentMethodMVP.Model model;

    public PaymentMethodPresenter(PaymentMethodMVP.Model model) {//dagger va a inyectar el model
        this.model = model;
    }

    @Override
    public void setView(PaymentMethodMVP.View view) {
        this.view = view;
    }

    @Override
    public void getPaymentMethods() {
        view.showProgress();
        model.getPaymentMethods(this);
    }

    @Override
    public void nextButtonClick() {
        if (paymentMethodSelected()) {
            view.goBanksActivity();
        }else{
            view.showInvalidMethodDialog();
        }
    }

    private boolean paymentMethodSelected() {
        String pm = view.getSelectedPaymentMethod();
        if (pm == null || pm.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void onFinished(ArrayList<PaymentMethod> paymentMethodsArrayList) {
        view.updatePaymentMethods(paymentMethodsArrayList);
        view.hideProgress();
    }

    @Override
    public void onFailure(Throwable t) {
        view.showErrorDialog(t);
        view.hideProgress();
    }
}
