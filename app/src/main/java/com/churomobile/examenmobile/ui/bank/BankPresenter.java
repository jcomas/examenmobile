package com.churomobile.examenmobile.ui.bank;

import android.support.annotation.Nullable;

import com.churomobile.examenmobile.http.entity.Bank;

import java.util.ArrayList;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class BankPresenter implements BankMVP.Presenter, BankMVP.Model.OnFinishedListener {

    @Nullable
    private BankMVP.View view;
    private BankMVP.Model model;

    public BankPresenter(BankMVP.Model model) {//dagger va a inyectar el model
        this.model = model;
    }

    @Override
    public void setView(BankMVP.View view) {
        this.view = view;
    }

    @Override
    public void getBanks() {
        view.showProgress();
        String paymentMethod = view.getPaymentMethod();
        if (paymentMethod == null) {
            view.showInvalidPaymentMethodError();
        } else {
            model.getBanks(paymentMethod, this);
        }

    }

    @Override
    public void nextButtonClick() {
        if(validBank()) {
            view.goInstallmentsActivity();
        }else{
            view.showNoSelectedBankError();
        }
    }

    private boolean validBank() {
        String pm = view.getSelectedBank();
        if (pm == null || pm.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void onFinished(ArrayList<Bank> bankList) {
        view.updateBanks(bankList);
        view.hideProgress();
        if (bankList.size() == 0) {
            view.showNoResultError();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        view.showErrorDialog(t);
        view.hideProgress();
    }
}
