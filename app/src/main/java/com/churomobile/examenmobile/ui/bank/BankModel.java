package com.churomobile.examenmobile.ui.bank;

import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.MercadoPagoAPI;
import com.churomobile.examenmobile.http.entity.Banks;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class BankModel implements BankMVP.Model {

    private final MercadoPagoAPI service;

    public BankModel(MercadoPagoAPI service) {
        this.service = service;
    }

    @Override
    public void getBanks(String paymentMethod, BankMVP.Model.OnFinishedListener listener) {

        Call<Banks> call = this.service.getBanks(Constants.MP_PUBLIC_KEY, paymentMethod);
        call.enqueue(new Callback<Banks>() {
            @Override
            public void onResponse(Call<Banks> call, Response<Banks> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<Banks> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }

}
