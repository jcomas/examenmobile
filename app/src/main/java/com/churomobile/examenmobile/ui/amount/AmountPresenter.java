package com.churomobile.examenmobile.ui.amount;

import android.support.annotation.Nullable;

/**
 * Created by jecom_000 on 30/11/2018....
 */
public class AmountPresenter implements AmountMVP.Presenter {

    @Nullable
    private AmountMVP.View view;

    @Override
    public void setView(AmountMVP.View view) {
        this.view = view;
    }

    @Override
    public String getAmount() {
        if (view != null) {
            return view.getAmount();
        }
        return null;
    }

    @Override
    public void nextButtonClick() {
        if (view != null) {
            String amount = view.getAmount();
            if (amount == null || amount.isEmpty() || !isValid(amount)) {
                view.showAmountError();
            } else {
                view.goPaymentMethodsActivity(amount);
            }
        }
    }

    @Override
    public void clearData() {
        view.clearData();
    }

    public boolean isValid(String amount) {
        boolean isValid;
        try {
            double result = Double.parseDouble(amount);
            isValid = result > 0;
        } catch (Exception ex) {
            isValid = false;
        }
        return isValid;
    }
}
