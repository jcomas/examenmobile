package com.churomobile.examenmobile.ui.bank;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.http.entity.Bank;

import java.util.List;

/**
 * Created by jecom_000 on 2/12/2018....
 */
public class BankAdapter extends RecyclerView.Adapter<BankAdapter.BankHolder> {

    Context context;
    List<Bank> banksList;
    BankAdapter.OnItemClickListener listener;

    public BankAdapter(Context context, List<Bank> banksList, BankAdapter.OnItemClickListener listener) {
        this.context = context;
        this.banksList = banksList;
        this.listener = listener;
    }

    public void setBanksList(List<Bank> banksList) {
        this.banksList = banksList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BankHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.bank_adapter, parent, false);
        return new BankHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BankHolder bankHolder, int position) {
        bankHolder.tvBank.setText(banksList.get(position).getName());
        Glide.with(context).load(banksList.get(position).getThumbnail()).into(bankHolder.image);
        bankHolder.bind(banksList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return banksList.size();
    }

    public class BankHolder extends RecyclerView.ViewHolder {
        TextView tvBank;
        ImageView image;

        public BankHolder(View itemView) {
            super(itemView);
            tvBank = itemView.findViewById(R.id.textViewBank);
            image = itemView.findViewById(R.id.imageViewBank);
        }

        public void bind(final Bank item, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Bank bank);
    }
}
