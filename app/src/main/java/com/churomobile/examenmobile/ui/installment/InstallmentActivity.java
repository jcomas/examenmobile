package com.churomobile.examenmobile.ui.installment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.churomobile.examenmobile.MyApplication;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.entity.PayerCost;
import com.churomobile.examenmobile.ui.amount.AmountActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class InstallmentActivity extends AppCompatActivity implements InstallmentMVP.View {

    @Inject
    InstallmentMVP.Presenter presenter;

    private List<PayerCost> banksList;
    private RecyclerView recyclerView;
    private InstallmentAdapter recyclerAdapter;
    private Button nextButton;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installment);

        ((MyApplication) getApplication()).getComponent().inject(this);

        nextButton = findViewById(R.id.buttonFinish);
        nextButton.setOnClickListener(view -> presenter.finishButtonClick());
        nextButton.setVisibility(View.GONE);

        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.retrieving_payment_method));

        banksList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerViewInstallments);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new InstallmentAdapter(this, banksList, payerCost -> {

            Bundle bundle = getIntent().getExtras();
            bundle.putString(Constants.PAYER_COST, String.valueOf(payerCost.getInstallments()));
            getIntent().putExtras(bundle);

            presenter.finishButtonClick();

//            showDialog("",payerCost.getRecommendedMessage());

        });
        recyclerView.setAdapter(recyclerAdapter);

    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.getPayerCosts();

    }

    @Override
    public void updatePayerCosts(ArrayList<PayerCost> payerCostList) {
        recyclerAdapter.setPayerCostList(payerCostList);
    }

    @Override
    public void showErrorDialog(Throwable t) {
        showDialog(getString(R.string.error), getString(R.string.retrieving_error));
    }

    @Override
    public void showInvalidPaymentMethodError() {
        showDialog(getString(R.string.error), getString(R.string.invalid_payment_method));
    }

    @Override
    public void showInvalidBankError() {
        showDialog(getString(R.string.error), getString(R.string.invalid_bank_error));
    }

    @Override
    public void showNoResultError() {
        showDialog(getString(R.string.error), getString(R.string.no_result));
    }

    @Override
    public void showInvalidAmount() {
        showDialog(getString(R.string.error), getString(R.string.invalid_amount));
    }

    @Override
    public String getPaymentMethod() {
        return getIntent().getStringExtra(Constants.PAYMNET_METHOD_ID);
    }

    @Override
    public String getIssuerBank() {
        return getIntent().getStringExtra(Constants.BANK_ID);
    }

    @Override
    public String getAmount() {
        return getIntent().getStringExtra(Constants.AMOUNT);
    }

    @Override
    public void showSuccessDialog() {
        Intent intent = new Intent(this, AmountActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    @Override
    public String getInstallment() {
        String installment = getIntent().getStringExtra(Constants.PAYER_COST);
        return installment;
    }

    @Override
    public void showInvalidInstallmentError() {
        showDialog(getString(R.string.error), getString(R.string.invalid_installment));
    }

    @Override
    public void showProgress() {
        progress.show();
    }

    @Override
    public void hideProgress() {
        progress.dismiss();
    }
}
