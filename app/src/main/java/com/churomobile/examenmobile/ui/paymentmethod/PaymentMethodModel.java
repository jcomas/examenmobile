package com.churomobile.examenmobile.ui.paymentmethod;

import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.MercadoPagoAPI;
import com.churomobile.examenmobile.http.entity.PaymentMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jc on 30/11/2018....
 */
public class PaymentMethodModel implements PaymentMethodMVP.Model {

    private final MercadoPagoAPI service;

    public PaymentMethodModel(MercadoPagoAPI service) {
        this.service = service;
    }

    @Override
    public void getPaymentMethods(OnFinishedListener listener) {

        Call<PaymentMethods> call = this.service.getPaymentMethods(Constants.MP_PUBLIC_KEY);
        call.enqueue(new Callback<PaymentMethods>() {
            @Override
            public void onResponse(Call<PaymentMethods> call, Response<PaymentMethods> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PaymentMethods> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }


}
