package com.churomobile.examenmobile.ui.bank;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.churomobile.examenmobile.MyApplication;
import com.churomobile.examenmobile.R;
import com.churomobile.examenmobile.common.Constants;
import com.churomobile.examenmobile.http.entity.Bank;
import com.churomobile.examenmobile.ui.installment.InstallmentActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BankActivity extends AppCompatActivity implements BankMVP.View {


    @Inject
    BankMVP.Presenter presenter;

    private List<Bank> banksList;
    private RecyclerView recyclerView;
    private BankAdapter recyclerAdapter;
    private Button nextButton;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banks);

        ((MyApplication) getApplication()).getComponent().inject(this);

        nextButton = findViewById(R.id.buttonNext);
        nextButton.setOnClickListener(view -> presenter.nextButtonClick());
        nextButton.setVisibility(View.GONE);

        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.retrieving_payment_method));

        banksList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerViewBanks);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new BankAdapter(this, banksList, bank -> {

            Bundle bundle = getIntent().getExtras();
            bundle.putString(Constants.BANK_ID, bank.getId());
            bundle.putString(Constants.BANK, bank.getName());
            getIntent().putExtras(bundle);

            presenter.nextButtonClick();

//            showDialog("", bank.getName());

        });
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.getBanks();
    }

    @Override
    public void updateBanks(ArrayList<Bank> banksList) {
        recyclerAdapter.setBanksList(banksList);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showErrorDialog(Throwable t) {
        t.printStackTrace();
        showDialog(getString(R.string.error), getString(R.string.retrieving_error));
    }

    @Override
    public void showInvalidPaymentMethodError() {
        showDialog(getString(R.string.error), getString(R.string.invalid_payment_method));
    }

    @Override
    public void showNoResultError() {
        showDialog(getString(R.string.error), getString(R.string.no_result));
    }

    @Override
    public void goInstallmentsActivity() {
        Intent intent = new Intent(this, InstallmentActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    @Override
    public String getPaymentMethod() {
        return getIntent().getStringExtra(Constants.PAYMNET_METHOD_ID);
    }

    @Override
    public String getSelectedBank() {
        String sb = getIntent().getStringExtra(Constants.BANK_ID);
        return sb;
    }

    @Override
    public void showNoSelectedBankError() {
        showDialog(getString(R.string.error), getString(R.string.invalid_bank_error));
    }

    @Override
    public void showProgress() {
        progress.show();
    }

    @Override
    public void hideProgress() {
        progress.dismiss();
    }

}
