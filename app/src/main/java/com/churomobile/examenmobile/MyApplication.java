package com.churomobile.examenmobile;

import android.app.Application;

import com.churomobile.examenmobile.dagger.AmountModule;
import com.churomobile.examenmobile.dagger.BankModule;
import com.churomobile.examenmobile.dagger.PaymentMethodModule;

public class MyApplication extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .amountModule(new AmountModule())
                .paymentMethodModule(new PaymentMethodModule())
                .bankModule(new BankModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }

}
