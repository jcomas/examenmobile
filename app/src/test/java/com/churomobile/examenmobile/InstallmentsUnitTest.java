package com.churomobile.examenmobile;

import com.churomobile.examenmobile.http.entity.Installment;
import com.churomobile.examenmobile.ui.installment.InstallmentMVP;
import com.churomobile.examenmobile.ui.installment.InstallmentPresenter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jecom_000 on 4/12/2018....
 */
public class InstallmentsUnitTest implements InstallmentMVP.Model.OnFinishedListener {

    InstallmentPresenter presenter;

    InstallmentMVP.Model mockedModel;
    InstallmentMVP.View mockedView;

    @Before
    public void init() {
        mockedModel = mock(InstallmentMVP.Model.class);
        mockedView = mock(InstallmentMVP.View.class);

        presenter = new InstallmentPresenter(mockedModel);
        presenter.setView(mockedView);
    }

    @Test
    public void getInstallmentsWithInvalidPaymentMehodData() {
        String paymentMethod = null;
        String issuer = "1";
        String amount = "500";
        when(mockedView.getPaymentMethod()).thenReturn(paymentMethod);
        when(mockedView.getIssuerBank()).thenReturn(issuer);
        when(mockedView.getAmount()).thenReturn(amount);
        presenter.getPayerCosts();
        verify(mockedView).showInvalidPaymentMethodError();
    }

    @Test
    public void getInstallmentsWithInvalidIssuerData() {
        String paymentMethod = "visa";
        String issuer = null;
        String amount = "500";
        when(mockedView.getPaymentMethod()).thenReturn(paymentMethod);
        when(mockedView.getIssuerBank()).thenReturn(issuer);
        when(mockedView.getAmount()).thenReturn(amount);
        presenter.getPayerCosts();
        verify(mockedView).showInvalidBankError();
    }

    @Test
    public void getInstallmentsWithInvalidIssuerAmountData() {
        String paymentMethod = "visa";
        String issuer = "1";
        String amount = null;
        when(mockedView.getPaymentMethod()).thenReturn(paymentMethod);
        when(mockedView.getIssuerBank()).thenReturn(issuer);
        when(mockedView.getAmount()).thenReturn(amount);
        presenter.getPayerCosts();
        verify(mockedView).showInvalidAmount();
    }

    @Test
    public void getInstallmentsWithValidData() {
        String paymentMethod = "visa";
        String issuer = "1";
        String amount = "50";
        when(mockedView.getPaymentMethod()).thenReturn(paymentMethod);
        when(mockedView.getIssuerBank()).thenReturn(issuer);
        when(mockedView.getAmount()).thenReturn(amount);
        presenter.getPayerCosts();
        verify(mockedModel).getPayerCosts(amount,paymentMethod,issuer,this);
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }

    @Override
    public int hashCode() {
        return 9999;
    }

    @Test
    public void nextButtonClickWithInvalidData() {
        when(mockedView.getInstallment()).thenReturn(null);
        presenter.finishButtonClick();
        verify(mockedView).showInvalidInstallmentError();
    }

    @Test
    public void nextButtonClickWithValidData() {
        when(mockedView.getInstallment()).thenReturn("1");
        presenter.finishButtonClick();
        verify(mockedView).showSuccessDialog();
    }

    @Override
    public void onFinished(ArrayList<Installment> installmentList) {

    }

    @Override
    public void onFailure(Throwable t) {

    }
}
