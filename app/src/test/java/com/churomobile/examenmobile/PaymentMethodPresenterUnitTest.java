package com.churomobile.examenmobile;

import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodMVP;
import com.churomobile.examenmobile.ui.paymentmethod.PaymentMethodPresenter;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jecom_000 on 4/12/2018....
 */
public class PaymentMethodPresenterUnitTest {

    PaymentMethodPresenter presenter;

    PaymentMethodMVP.Model mockedModel;
    PaymentMethodMVP.View mockedView;

    @Before
    public void init() {
        mockedModel = mock(PaymentMethodMVP.Model.class);
        mockedView = mock(PaymentMethodMVP.View.class);

        presenter = new PaymentMethodPresenter(mockedModel);
        presenter.setView(mockedView);
    }

    @Test
    public void nextButtonClickWithInvalidData() {
        when(mockedView.getSelectedPaymentMethod()).thenReturn(null);
        presenter.nextButtonClick();
        verify(mockedView).showInvalidMethodDialog();
    }

    @Test
    public void nextButtonClickWithValidData() {
        when(mockedView.getSelectedPaymentMethod()).thenReturn("1");
        presenter.nextButtonClick();
        verify(mockedView).goBanksActivity();
    }

}
