package com.churomobile.examenmobile;

import com.churomobile.examenmobile.ui.amount.AmountMVP;
import com.churomobile.examenmobile.ui.amount.AmountPresenter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AmountPresenterUnitTest {

    AmountPresenter presenter;

    AmountMVP.Model mockedModel;
    AmountMVP.View mockedView;

    @Before
    public void init() {
        mockedModel = mock(AmountMVP.Model.class);
        mockedView = mock(AmountMVP.View.class);

        presenter = new AmountPresenter();
        presenter.setView(mockedView);
    }

    @Test
    public void amountZeroIsIncorrect() {
        assertFalse(presenter.isValid("0"));
    }

    @Test
    public void amountNegativeIsIncorrect() {
        assertFalse(presenter.isValid("-1"));
    }

    @Test
    public void amountPositiveIsIncorrect() {
        assertTrue(presenter.isValid("0.1"));
    }

    @Test
    public void nextButtonClickWithInvalidData() {
        String value = "-1";
        when(mockedView.getAmount()).thenReturn(value);
        presenter.nextButtonClick();
        verify(mockedView).showAmountError();
    }

    @Test
    public void nextButtonClickWithValidData() {
        String value = "1";
        when(mockedView.getAmount()).thenReturn(value);
        presenter.nextButtonClick();
        verify(mockedView).goPaymentMethodsActivity(value);
    }
}