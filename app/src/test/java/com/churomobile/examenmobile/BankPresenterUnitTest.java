package com.churomobile.examenmobile;

import com.churomobile.examenmobile.http.entity.Bank;
import com.churomobile.examenmobile.ui.bank.BankMVP;
import com.churomobile.examenmobile.ui.bank.BankPresenter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jecom_000 on 4/12/2018....
 */
public class BankPresenterUnitTest implements BankMVP.Model.OnFinishedListener {

    BankPresenter presenter;

    BankMVP.Model mockedModel;
    BankMVP.View mockedView;

    @Before
    public void init() {
        mockedModel = mock(BankMVP.Model.class);
        mockedView = mock(BankMVP.View.class);

        presenter = new BankPresenter(mockedModel);
        presenter.setView(mockedView);
    }

    @Test
    public void getBanksWithInvalidPaymentMehodData() {
        when(mockedView.getPaymentMethod()).thenReturn(null);
        presenter.getBanks();
        verify(mockedView).showInvalidPaymentMethodError();
    }

    @Test
    public void getBanksWithValidPaymentMehodData() {
        when(mockedView.getPaymentMethod()).thenReturn("visa");
        presenter.getBanks();
        verify(mockedModel).getBanks("visa", this);
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }

    @Override
    public int hashCode() {
        return 9999;
    }

    @Test
    public void nextButtonClickWithInvalidData() {
        when(mockedView.getSelectedBank()).thenReturn(null);
        presenter.nextButtonClick();
        verify(mockedView).showNoSelectedBankError();
    }

    @Test
    public void nextButtonClickWithValidData() {
        when(mockedView.getSelectedBank()).thenReturn("1");
        presenter.nextButtonClick();
        verify(mockedView).goInstallmentsActivity();
    }


    @Override
    public void onFinished(ArrayList<Bank> banksList) {

    }

    @Override
    public void onFailure(Throwable t) {

    }
}
